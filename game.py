# ---------------------------------------------------- #

from random import randint

# ---------------------------------------------------- #

# Define Global Option Variables

# ---------------------------------------------------- #

# String list of names of Months
MonthData = [

    "Indexuary",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]

# String table of printed strings.
StringData = {

    "NamePrompt": "Hey, what's your name? ",
    "YesOrNoPrompt": "Yes, Earlier, or Later? ",
    "ComputerResponseCorrect": "I knew it!",
    "ComputerResponseIncorrect": "Drat! Lemme try again!",
    "ComputerResponseGiveUp": "I have other things to do. Goodbye..",
    "NewLineFormat": "=====================================\n",
}

# Integer variable for the number of times to loop.
LoopNumber = 10

LowRange, HighRange = 1924, 2004

# ---------------------------------------------------- #

# Define Global Variables
RandomYear, RandomMonth, Name, Response, GuessesLeft = None, None, None, None, None

# ---------------------------------------------------- #

Name = input(StringData["NamePrompt"])
GuessesLeft = LoopNumber

# ---------------------------------------------------- #

# For loop which will ask the question the set number of times with the variable LoopNumber
for Iteration in range(1, LoopNumber + 1):

    # -------------------------------- #
    RandomYear = (randint(LowRange, HighRange))
    RandomYearString = str(RandomYear)
    RandomMonth = MonthData[randint(1, 12)]
    FormattedDate = (RandomMonth + " / " + RandomYearString)
    FormattedGuess = (Name + ", were you born in: " + FormattedDate)

    # -------------------------------- #

    print("Guess#: " , Iteration)
    print(FormattedGuess)
    Response = str.lower(input(StringData["YesOrNoPrompt"]))

    # -------------------------------- #

    if Response == "yes":

        print(StringData["NewLineFormat"] + StringData["ComputerResponseCorrect"])
        print("=====================================")

        break

    elif Response == "later":

        if Iteration == LoopNumber:
            print(StringData["NewLineFormat"] + StringData["ComputerResponseGiveUp"])
        else:
            print(StringData["NewLineFormat"] + StringData["ComputerResponseIncorrect"])
            LowRange = (RandomYear + 1)

            if LowRange == HighRange:
                LowRange = RandomYear

    elif Response == "earlier":

        if Iteration == LoopNumber:
            print(StringData["NewLineFormat"] + StringData["ComputerResponseGiveUp"])
        else:
            print(StringData["NewLineFormat"] + StringData["ComputerResponseIncorrect"])
            HighRange = (RandomYear - 1)

            if HighRange == LowRange:
                HighRange = RandomYear
    else:

        if Iteration == LoopNumber:
            print(StringData["NewLineFormat"] + StringData["ComputerResponseGiveUp"])
        else:
            print(StringData["NewLineFormat"] + "Respond Properly..")

    print("=====================================")

# ---------------------------------------------------- #
